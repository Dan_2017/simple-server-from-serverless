require('@babel/register');
const path = require('path');
const webpack = require('webpack')
const HtmlWebpackPlugin = require('html-webpack-plugin');
const CopyWebpackPlugin = require('copy-webpack-plugin');
const Dotenv = require('dotenv-webpack');

module.exports = (env, argv) => {
  const mode = argv.mode || 'development';
  console.log('[WEBPACK]', env, argv)
  return {
    entry: {
      index: './src/client/index.js',
      // serverless: './src/client/services/serverless.js',
    },
    output: {
      filename: '[name].js',
      // path: path.resolve(__dirname, 'public'),
      path: path.resolve(__dirname, 'dist/static'),
    },
    resolve: {
      extensions: ['.ts', '.tsx', '.js'],
      alias: {
        Styles: path.resolve(__dirname, 'src/client/assets/styles/'),
        Images: path.resolve(__dirname, 'src/client/assets/images/'),
        Fonts: path.resolve(__dirname, 'src/client/assets/fonts/'),
        Lib: path.resolve(__dirname, 'src/client/libraries/'),
        Srv: path.resolve(__dirname, 'src/client/services/'),
      },
    },
    module: {
      rules: [
        {
          test: /\.(ts|tsx)$/,
          exclude: /node_modules/,
          use: 'ts-loader',
        },
        {
          test: /\.js$/,
          exclude: /node_modules/,
          use: 'babel-loader',
        },
        {
          test: /\.(le|c)ss$/,
          use: ['style-loader', 'css-loader', 'less-loader'],
        },
        {
          test: /\.(png|svg|jpg|jpeg|gif|ico)$/i,
          type: 'asset/resource',
        },
        {
          test: /\.(woff|woff2|eot|ttf|otf)$/i,
          type: 'asset/resource',
        },
        {
          test: /\.(webp)$/i,
          use: 'file-loader',
        }
      ],
    },
    plugins: [
      new webpack.DefinePlugin({
        'process.env.NODE_ENV': JSON.stringify(mode),
      }),
      new Dotenv(),
      new HtmlWebpackPlugin({
        title: 'Static Serverless Page',
        SEO: require('./src/client/components/seo').default([{name:"generator",content:"webpack"}]),
        filename: 'index.html',
        template: 'src/client/assets/templates/index.html',
        chunks: ['index'],
        minify: {
          collapseWhitespace: false,
          preserveLineBreaks: true,
        },
      }),
      new CopyWebpackPlugin({
        patterns: [
          {
            from: path.resolve(__dirname, 'src/client/assets/images/icons/favicon.ico'),
            to: path.resolve(__dirname, 'public'),
          },
        ],
      })
    ],
    devServer: {
      static: './public',
    },
    performance: {
      maxAssetSize: 550000,
      maxEntrypointSize: 550000,
    }
  }
};
