# Simple React SSR server

This server uses NodeJS, React SSR and NodeJS http module (No ExpressJS).

## Installation

- ```npm install```

## Run locally

- ```npm run dev:srv```

## Run in Docker

- ```docker build -t simple-server .```
- ```docker run -p 3007:3007 -d simple-server ```

Then open http://localhost:3007 in your browser
