import React, { useState } from 'react';

const MainLayout = (props) => {
  const { title, content, meta, base } = props;
  const [metas, setMetas] = useState([
    { name: "viewport", content: "width=device-width, initial-scale=1.0" },
    { name: "title", content: "title" },
    { name: "description", content: "description" }
  ]);

  // const _base = 'http://localhost:8080'

  const metaTags = [...metas, ...meta].map((m, i) => (
    <meta name={m.name} content={m.content} key={i} />
  ));

  return (
    <html>
      <head>
        <title>{title}</title>
        <meta charSet="UTF-8" />
        {metaTags}
        <link rel="icon" type="image/ico" href="/favicon.ico" id="favicon"></link>
        <base href={base}></base>
      </head>
      <body>
        <header>
          <h1>You are here: {title}</h1>
        </header>
        <nav>
          <a href="/">Home</a> |
          <a href="/page/1">Page 1</a> |
          <a href="/page/100">Page 100</a>
        </nav>
        <main>
          {JSON.stringify(content)}
        </main>
        <footer>
          <p>© Год</p>
        </footer>
        <div id="posts" data-param1="PARAM_1" data-param2={btoa(JSON.stringify(content))}></div>
        <script src="/index.js"></script>
      </body>
    </html>
  );
};

export const PostsLayout = (props) => {
  const { title, content, meta, base } = props;
  const [metas, setMetas] = useState([
    { name: "viewport", content: "width=device-width, initial-scale=1.0" },
    { name: "title", content: "title" },
    { name: "description", content: "description" }
  ]);

  // const _base = 'http://localhost:8080'

  const metaTags = [...metas, ...meta].map((m, i) => (
    <meta name={m.name} content={m.content} key={i} />
  ));

  const posts = content.map((post, index) => {
    return (
      <>
        <article key={index}>{'\n'}
          <h2><a href={`/posts/${post.id}`}>{post.title}</a></h2>{'\n'}
          <p>{post.body}</p>{'\n'}
          <p><a href={`/posts/${post.id}`}>Read more</a></p>{'\n'}
        </article>{'\n'}
      </>
    )
  })

  return (
    <html>
      <head>
        <title>{title}</title>
        <meta charSet="UTF-8" />
        {metaTags}
        <link rel="icon" type="image/ico" href="/favicon.ico" id="favicon"></link>
        <base href={base}></base>
      </head>
      <body>
        <header>
          <h1>{title}</h1>
        </header>
        <nav>
          <a href="/">Home</a> |
          <a href="/page/1">Page 1</a> |
          <a href="/page/100">Page 100</a>
        </nav>
        <main>
          <div id="posts" data-content={btoa(JSON.stringify(content))}>
            {posts}
          </div>
        </main>
        <footer>
          <p>© Год</p>
        </footer>
        <script src="/index.js"></script>
      </body>
    </html>
  );
};

export const PostLayout = (props) => {
  const { title, content, meta, base } = props;
  const [metas, setMetas] = useState([
    { name: "viewport", content: "width=device-width, initial-scale=1.0" },
    { name: "title", content: "title" },
    { name: "description", content: "description" }
  ]);

  // const _base = 'http://localhost:8080'

  const metaTags = [...metas, ...meta].map((m, i) => (
    <meta name={m.name} content={m.content} key={i} />
  ));

  const comments = content.commentsData.map((comment, index) => {
    return (
      <>
        <div key={index}>{'\n'}
          <h4><a href={`mailto:${comment.email}`}>{comment.name}</a></h4>{'\n'}
          <p>{comment.body}</p>{'\n'}
          <p><a href={`mailto:${comment.email}`}>{comment.email}</a></p>{'\n'}
        </div>{'\n'}
      </>
    )
  })

  return (
    <html>
      <head>
        <title>{title}</title>
        <meta charSet="UTF-8" />
        {metaTags}
        <link rel="icon" type="image/ico" href="/favicon.ico" id="favicon"></link>
        <base href={base}></base>
      </head>
      <body>
        <header>
          <h1>{title}</h1>
        </header>
        <nav>
          <a href="/">Home</a> |
          <a href="/page/1">Page 1</a> |
          <a href="/page/100">Page 100</a>
        </nav>
        <main>
          <div id="post" data-content={btoa(JSON.stringify(content))}>
            <h2>{content.postData.title}</h2>
            <p>{content.postData.body}</p>
            <h3>Comments:</h3>
            {comments}
          </div>
        </main>
        <footer>
          <p>© Год</p>
        </footer>
        <script src="/index.js"></script>
      </body>
    </html>
  );
};

export const AlbumLayout = (props) => {
  const { title, content, meta, base } = props;
  const [metas, setMetas] = useState([
    { name: "viewport", content: "width=device-width, initial-scale=1.0" },
    { name: "title", content: "title" },
    { name: "description", content: "description" }
  ]);

  // const _base = 'http://localhost:8080'

  const metaTags = [...metas, ...meta].map((m, i) => (
    <meta name={m.name} content={m.content} key={i} />
  ));

  return (
    <html>
      <head>
        <title>{title}</title>
        <meta charSet="UTF-8" />
        {metaTags}
        <link rel="icon" type="image/ico" href="/favicon.ico" id="favicon"></link>
        <base href={base}></base>
      </head>
      <body>
        <header>
          <h1>You are here: {title}</h1>
        </header>
        <nav>
          <a href="/">Home</a> |
          <a href="/page/1">Page 1</a> |
          <a href="/page/100">Page 100</a>
        </nav>
        <main>
          {JSON.stringify(content)}
        </main>
        <footer>
          <p>© Год</p>
        </footer>
        <div id="posts" data-param1="PARAM_1" data-param2={btoa(JSON.stringify(content))}></div>
        <script src="/index.js"></script>
      </body>
    </html>
  );
};

export default MainLayout;