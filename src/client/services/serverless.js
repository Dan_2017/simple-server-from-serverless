import URL from 'url-parse';
import { router } from './rest.js';

let count = 0;

self.addEventListener('activate', function (event) {
  console.log('Service worker activated')
});

const broadcast = new BroadcastChannel('count-channel');

broadcast.onmessage = (event) => {
  if (event.data && event.data.type === 'INCREASE_COUNT') {
    // broadcast.postMessage({ payload: `Service requests: ${++count}` });
  }
  if (event.data && event.data.type === 'MESSAGE') {
    broadcast.postMessage({ payload: `${count}: Echo:${event.data.payload}` });
  }
};

self.addEventListener('fetch', function (event) {
  event.respondWith(handleRequest(event.request));
  event.preventDefault();
});

async function handleRequest(request) {
  const parsedURL = new URL(request.url)
  const route = router.match(parsedURL.pathname);

  if (route) {
    return new Response(route.callback(route), {
      headers: { 'Content-Type': 'text/html' },
    });
    // return route.callback(route)
  }

  return fetch(request);
}
