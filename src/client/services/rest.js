import React from 'react';
import ReactDOMServer from 'react-dom/server';
import Router from '../libraries/router';
import MainLayout from '../layouts/main';
import PostsLayout from '../layouts/posts';
import PostLayout from '../layouts/post';
import AlbumsLayout from '../layouts/albums';
import AlbumLayout from '../layouts/album';

const router = new Router()
router.addRoute('/page/:pageid', async ({ params }) => {
  const response = await fetch(`https://jsonplaceholder.typicode.com/posts/${params.pageid}`)
  const json = await response.json()

  const page = renderPage(null, request.url, json, parsedURL.protocol + parsedURL.hostname + (parsedURL.port ? `:${parsedURL.port}` : ''))
  // return new Response(page, {
  //   headers: { 'Content-Type': 'text/html' },
  // });
  return page;
});

router.addRoute('/user/:userid/posts', async ({ params }) => {
  const user = await fetch(`https://jsonplaceholder.typicode.com/users/${params.userid}`)
  const userData = await user.json()

  const posts = await fetch(`https://jsonplaceholder.typicode.com/users/${params.userid}/posts/`)
  const json = await posts.json()

  const base = parsedURL.protocol + parsedURL.hostname + (parsedURL.port ? `:${parsedURL.port}` : '')

  const title = `${userData.name} blog posts`

  const bcmenu = [{title:'Home', href:"/"}, {title:`${userData.name} blog`, href:'.'}]

  const page = renderPosts(bcmenu, title , json, base)

  // return new Response(page, {
  //   headers: { 'Content-Type': 'text/html' },
  // });
  return page;
});

router.addRoute('/posts/:postid', async ({ params }) => {
  const post = await fetch(`https://jsonplaceholder.typicode.com/posts/${params.postid}`)
  const postData = await post.json()

  const comments = await fetch(`https://jsonplaceholder.typicode.com/posts/${params.postid}/comments/`)
  const commentsData = await comments.json()

  const user = await fetch(`https://jsonplaceholder.typicode.com/users/${postData.userId}`)
  const userData = await user.json()

  const base = parsedURL.protocol + parsedURL.hostname + (parsedURL.port ? `:${parsedURL.port}` : '')

  const title = `${userData.name} blog`

  const bcmenu = [{title:'Home', href:"/"}, {title:`${userData.name} blog`, href:`/user/${postData.userId}/posts`}, {title:`${postData.title}`, href:"."}]

  const page = renderPost(bcmenu, title, {postData, commentsData}, base)

  // return new Response(page, {
  //   headers: { 'Content-Type': 'text/html' },
  // });
  return page;
});

router.addRoute('/user/:userid/albums', async ({ params }) => {
  const user = await fetch(`https://jsonplaceholder.typicode.com/users/${params.userid}`)
  const userData = await user.json()

  const albums = await fetch(`https://jsonplaceholder.typicode.com/users/${params.userid}/albums/`)
  const json = await albums.json()

  const base = parsedURL.protocol + parsedURL.hostname + (parsedURL.port ? `:${parsedURL.port}` : '')

  const title = `${userData.name} photo albums`

  const bcmenu = [{title:'Home', href:"/"}, {title:`${userData.name} photo albums`, href:'.'}]

  const page = renderAlbums(bcmenu, title, json, base)

  // return new Response(page, {
  //   headers: { 'Content-Type': 'text/html' },
  // });
  return page;
});

router.addRoute('/albums/:albumid', async ({ params }) => {
  const album = await fetch(`https://jsonplaceholder.typicode.com/albums/${params.albumid}`)
  const albumData = await album.json()

  const photos = await fetch(`https://jsonplaceholder.typicode.com/albums/${params.albumid}/photos/`)
  const photosData = await photos.json()

  const user = await fetch(`https://jsonplaceholder.typicode.com/users/${albumData.userId}`)
  const userData = await user.json()

  const base = parsedURL.protocol + parsedURL.hostname + (parsedURL.port ? `:${parsedURL.port}` : '')

  const title = `${userData.name} album`

  const bcmenu = [{title:'Home', href:"/"}, {title:`${userData.name} photo albums`, href:`/user/${albumData.userId}/albums`}, {title:`${albumData.title}`, href:"."}]

  const page = renderAlbum(bcmenu, title, {albumData, photosData}, base)

  // return new Response(page, {
  //   headers: { 'Content-Type': 'text/html' },
  // });
  return page;
});

function renderPage(metas, title, json, base) {
  const _metas = metas ? metas : [
    { name: 'keywords', content: 'SSR' },
  ];
  const content = ReactDOMServer.renderToString(
    <MainLayout title={title ? title : "__TITLE__"} content={json} meta={_metas} base={base} />
  );

  return `<!DOCTYPE html>${content.replace(/&quot;/g, '"')}`;
}

function renderPosts(menu, title, json, base) {
  const metas = [
    { name: 'keywords', content: 'posts' },
  ];
  const content = ReactDOMServer.renderToString(
    <PostsLayout title={title} menu={menu} content={json} meta={metas} base={base} />
  );
  return `<!DOCTYPE html>${content.replace(/&quot;/g, '"')}`;
}

function renderPost(menu, title, json, base) {
  const metas = [
    { name: 'keywords', content: 'blog post' },
  ];
  const content = ReactDOMServer.renderToString(
    <PostLayout title={title} menu={menu} content={json} meta={metas} base={base} />
  );

  return `<!DOCTYPE html>${content.replace(/&quot;/g, '"')}`;
}

function renderAlbums(menu, title, json, base) {
  const metas = [
    { name: 'keywords', content: 'albums' },
  ];
  const content = ReactDOMServer.renderToString(
    <AlbumsLayout title={title} menu={menu} content={json} meta={metas} base={base} />
  );

  return `<!DOCTYPE html>${content.replace(/&quot;/g, '"')}`;
}

function renderAlbum(menu, title, json, base) {
  const metas = [
    { name: 'keywords', content: 'photo album' },
  ];
  const content = ReactDOMServer.renderToString(
    <AlbumLayout title={title} menu={menu} content={json} meta={metas} base={base} />
  );

  return `<!DOCTYPE html>${content.replace(/&quot;/g, '"')}`;
}

export { router };