import React, { useState } from 'react';


const MainLayout = (props) => {
  const { title, content, meta, base } = props;
  const [metas, setMetas] = useState([
    { name: "viewport", content: "width=device-width, initial-scale=1.0" },
    { name: "title", content: "title" },
    { name: "description", content: "description" }
  ]);

  const metaTags = [...metas, ...meta].map((m, i) => (
    <meta name={m.name} content={m.content} key={i} />
  ));

  return (
    <html>
      <head>
        <title>{title}</title>
        <meta charSet="UTF-8" />
        {metaTags}
        <link rel="icon" type="image/ico" href="/favicon.ico" id="favicon"></link>
        <base href={base}></base>
      </head>
      <body>
        <header>
          <h1>You are here: {title}</h1>
        </header>
        <nav>
          <a href="/">Home</a> |
          <a href="/page/1">Page 1</a> |
          <a href="/page/100">Page 100</a>
        </nav>
        <main>
          {JSON.stringify(content)}
        </main>
        <footer>
          <p>© 2023</p>
        </footer>
      </body>
    </html>
  );
};

export default MainLayout;