import React from "react";
import { createRoot } from "react-dom/client";
import PostsList from './components/posts';
import UsersList from './components/users';
// import 'Lib/service';
import 'Styles/style.css';

const root = document.getElementById("root");
if (root) {
  createRoot(root).render(
    <UsersList />
  );
}

const posts = document.getElementById("posts");
if (posts) {
  const content = posts.getAttribute('data-content')
  posts.setAttribute('data-content', '[data-object]')
  createRoot(posts).render(
    <PostsList content={content} />
  );
}
