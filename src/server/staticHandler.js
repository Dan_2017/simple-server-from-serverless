import fs from 'fs';
import path from 'path';

function serveStatic(req, res) {
  const { url } = req;
  console.log('[url]', url)

  if (url === '/') {
    const indexPath = path.join(__dirname, '..', 'static', 'index.html');
    console.log('[indexPath]', indexPath)
    try {
      const fileContent = fs.readFileSync(indexPath);
      res.writeHead(200, { 'Content-Type': 'text/html' });
      res.end(fileContent);
      return true; // Вернуть true, если обработано статическое содержимое
    } catch (error) {
      // Если файл index.html не найден, продолжим дальше
    }
  }

  const staticPath = path.join(__dirname, '..', 'static', url);
  try {
    const fileContent = fs.readFileSync(staticPath);
    res.writeHead(200, { 'Content-Type': getContentType(staticPath) });
    res.end(fileContent);
    return true; // Вернуть true, если обработано статическое содержимое
  } catch (error) {
    // Если файл не найден, продолжим дальше
  }

  return false;
}

function getContentType(filePath) {
  const extname = path.extname(filePath);
  switch (extname) {
    case '.js':
      return 'text/javascript';
    case '.css':
      return 'text/css';
    case '.json':
      return 'application/json';
    case '.png':
      return 'image/png';
    default:
      return 'application/octet-stream';
  }
}

export default serveStatic;
