import http from 'http';
import URL from 'url-parse';
import serveStatic from './staticHandler.js';
import { router } from '../client/services/rest.js';
import fetch from 'node-fetch';

global.fetch = fetch;

global.btoa = function (str) {
  return Buffer.from(str).toString('base64');
};

global.atob = function (str) {
  return Buffer.from(str, 'base64').toString('utf-8');
};

const server = http.createServer(async (req, res) => {
  const { method, url } = req;
  global.parsedURL = new URL(url);
  console.log('[Parsed URL]:', parsedURL);
  if (serveStatic(req, res)) return;

  const route = router.match(url);

  if (route) {
    try {
      const response = await route.callback(route);
      res.writeHead(200, { 'Content-Type': 'text/html' });
      res.end(response);
    } catch (error) {
      console.error(error);
      res.writeHead(500, { 'Content-Type': 'text/plain' });
      res.end('Internal Server Error');
    }
  } else {
    res.writeHead(404, { 'Content-Type': 'text/plain' });
    res.end('Not Found');
  }
});

const PORT = 3007;
server.listen(PORT, () => {
  console.log(`Server running at http://localhost:${PORT}/`);
});
