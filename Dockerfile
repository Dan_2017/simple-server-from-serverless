# Используем официальный образ Node.js
FROM node:14.21.3

# Устанавливаем рабочую директорию
WORKDIR /var/page

# Копируем файлы приложения
COPY . .

# Устанавливаем зависимости
RUN npm install

# Экспортируем порт, на котором работает ваше приложение
EXPOSE 3007

# Команда для запуска сервера
CMD ["yarn", "start"]
